import random
import gym
import numpy as np
from collections import deque
# import plaidml.keras
# plaidml.keras.install_backend()
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from keras.layers import Dropout
from keras.models import load_model
from keras.models import model_from_json
from keras.regularizers import l2


from scores.score_logger import ScoreLogger

ENV_NAME = "CartPole-v1"

GAMMA = 0.95
LEARNING_RATE = 0.001

MEMORY_SIZE = 1000000
BATCH_SIZE = 40

EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.995

dropout = 0.1
regularizationFactor = 0.01
bias = True


class DQNSolver:

    def __init__(self, observation_space=None, action_space=None,jsonFile=None,h5File=None):
        self.exploration_rate = EXPLORATION_MAX
        self.action_space = action_space
        self.memory = deque(maxlen=MEMORY_SIZE)
        if (jsonFile==None):
            self.model = Sequential()
            self.model.add(Dense(40, input_shape=(observation_space,), activation="relu"))
            self.model.add(Dropout(dropout))
            # self.model.add(Dense(24, init='lecun_uniform', W_regularizer=l2(regularizationFactor), bias=bias))
            self.model.add(Dense(40, activation="relu"))
            self.model.add(Dropout(dropout))
            self.model.add(Dense(40, activation="relu"))
            self.model.add(Dropout(dropout))
            # self.model.add(Dense(8, activation="relu"))
            # self.model.add(Dropout(dropout))
            self.model.add(Dense(self.action_space, activation="linear"))
        else:
            json_file = open(jsonFile, 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            self.model = model_from_json(loaded_model_json)
            self.model.load_weights(h5File)
        self.model.compile(loss="mse", optimizer=Adam(lr=LEARNING_RATE))

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() < self.exploration_rate:
            return random.randrange(self.action_space)
        q_values = self.model.predict(state)
        return np.argmax(q_values[0])

    def actReplay(self,state):
        q_values = self.model.predict(state)
        return np.argmax(q_values[0])

    def experience_replay(self):
        if len(self.memory) < BATCH_SIZE:
            return
        batch = random.sample(self.memory, BATCH_SIZE)
        for state, action, reward, state_next, terminal in batch:
            q_update = reward
            if not terminal:
                q_update = (reward + GAMMA * np.amax(self.model.predict(state_next)[0]))
            q_values = self.model.predict(state)
            q_values[0][action] = q_update
            self.model.fit(state, q_values, verbose=0)
        self.exploration_rate *= EXPLORATION_DECAY
        self.exploration_rate = max(EXPLORATION_MIN, self.exploration_rate)

    def saveModelWeights(self,jsonFile1,h5File1):
        model_json = self.model.to_json()
        with open(jsonFile1, "w") as json_file:
            json_file.write(model_json)
        self.model.save_weights(h5File1)


def cartpole(jsonFile,h5File):
    env = gym.make(ENV_NAME)
    score_logger = ScoreLogger(ENV_NAME)
    observation_space = env.observation_space.shape[0]
    action_space = env.action_space.n
    dqn_solver = DQNSolver(observation_space=observation_space,action_space=action_space)
    run = 0
    while True:
        run += 1
        state = env.reset()
        state = np.reshape(state, [1, observation_space])
        step = 0
        while True:
            step += 1
            # env.render()
            action = dqn_solver.act(state)
            state_next, reward, terminal, info = env.step(action)
            reward = reward if not terminal else -reward
            state_next = np.reshape(state_next, [1, observation_space])
            dqn_solver.remember(state, action, reward, state_next, terminal)
            state = state_next
            if terminal:
                print ("Run: " + str(run) + ", exploration: " + str(dqn_solver.exploration_rate) + ", score: " + str(step))
                score_logger.add_score(step, run)
                dqn_solver.saveModelWeights(jsonFile,h5File)
                break
            dqn_solver.experience_replay()
    # dqn_solver.saveModelWeights(jsonFile,h5File)

def replay(jsonFile,h5File):
    env = gym.make(ENV_NAME)
    # score_logger = ScoreLogger(ENV_NAME)
    observation_space = env.observation_space.shape[0]
    action_space = env.action_space.n
    dqn_solver1 = DQNSolver(action_space=action_space,jsonFile=jsonFile,h5File=h5File)
    state = env.reset()
    nstate = state
    for _ in range(1000):        
        state = np.reshape(nstate, [1, observation_space])
        env.render()
        action = dqn_solver1.actReplay(state)
        # print (action)
        nstate, reward, done, info = env.step(action)
        if done:
            print("Episode finished after {} timesteps".format(_+1))
            break

    # state = env.reset()
    # for _ in range(1000):        
    #     state = np.reshape(state, [1, observation_space])
    #     env.render()
    #     state, reward, done, info = env.step(dqn_solver.act(state))
    #     if done:
    #         print("Episode finished after {} timesteps".format(_+1))
    #         break

    



if __name__ == "__main__":
    cartpole("model3.json","my_model_weights3.h5")
    # replay("model2.json",'my_model_weights2.h5')
